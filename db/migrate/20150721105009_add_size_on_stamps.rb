class AddSizeOnStamps < ActiveRecord::Migration
  def change
    add_column :stamps, :width, :float
    add_column :stamps, :height, :float
    rename_column :stamps, :stamp_id, :image_id
  end
end
