class AddStamps < ActiveRecord::Migration
  def change
    create_table :stamps do |t|
      t.references :label
      t.string :stamp_id
      t.float :top
      t.float :left

      t.timestamps
    end
  end
end
