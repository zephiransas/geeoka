class Label < ActiveRecord::Migration
  def change
    create_table :labels do |t|
      t.binary :data
      t.string :original_filename
      t.string :content_type

      t.timestamps
    end
  end
end
