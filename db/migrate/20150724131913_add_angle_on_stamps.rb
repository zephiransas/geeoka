class AddAngleOnStamps < ActiveRecord::Migration
  def change
    add_column :stamps, :angle, :float, default: 0
  end
end
