class LabelsController < ApplicationController
  def index
    @labels = Label.order(:id)
  end

  def new
    @label = Label.new
  end

  def upload
    @label = Label.new

    if params[:label]
      data = params[:label][:data]
      if data
        @label.original_filename = data.original_filename
        @label.content_type = data.content_type
        @label.data = data.read
      end
    end

    unless @label.save
      return render action: :new
    end

    redirect_to action: :edit, id: @label.id
  end

  def edit
    @label = Label.find_by_id(params[:id])
  end

  def update
    @label = Label.find_by_id(params[:id])
    stamps = JSON.parse(params[:stamp_json])

    ActiveRecord::Base.transaction do
      @label.stamps = []
      stamps.each do |stamp|
        @label.stamps << Stamp.new(
            image_id: stamp['image_id'],
            top: stamp['top'],
            left: stamp['left'],
            width: stamp['width'],
            height: stamp['height'],
            angle: stamp['angle']
        )
      end
      @label.save!
    end

    redirect_to action: :index
  end

  def destroy
    @label = Label.find_by_id(params[:id])
    @label.destroy
    redirect_to action: :index
  end

  def show_picture
    label = Label.find_by_id(params[:id])
    send_data(label.data, type: label.content_type, filename: label.original_filename)
  end

  def download
    label = Label.find_by_id(params[:id])
    image = Magick::Image.from_blob(label.data).first
    image.format = 'PNG'

    label.stamps.each do |stamp|
      stamp_image = Magick::Image.read("app/assets/images/stamps/#{stamp.image_id}").first
      stamp_image.background_color = "none"
      stamp_image.rotate!(stamp.angle * 180 / Math::PI)
      stamp_image.resize!(stamp.width, stamp.height)
      image.composite!(stamp_image, stamp.top, stamp.left, Magick::OverCompositeOp)
    end

    send_data(image.to_blob, disposition: :attachment, type: 'image/png', filename: "file_#{label.id}.png")
  end

end