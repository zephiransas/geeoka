class HomeController < ApplicationController
  def index
    redirect_to labels_path
  end
end