# == Schema Information
#
# Table name: stamps
#
#  id         :integer          not null, primary key
#  label_id   :integer
#  image_id   :string
#  top        :float
#  left       :float
#  created_at :datetime
#  updated_at :datetime
#  width      :float
#  height     :float
#

class Stamp < ActiveRecord::Base

  belongs_to :label

  def to_hash
    {
        stamp_id: self.id,
        image_id: self.image_id,
        top: self.top,
        left: self.left,
        width: self.width,
        height: self.height,
        angle: self.angle
    }
  end
end
