# == Schema Information
#
# Table name: labels
#
#  id                :integer          not null, primary key
#  data              :binary
#  original_filename :string
#  content_type      :string
#  created_at        :datetime
#  updated_at        :datetime
#

class Label < ActiveRecord::Base

  validates :data, presence: true
  has_many :stamps, dependent: :destroy

end
