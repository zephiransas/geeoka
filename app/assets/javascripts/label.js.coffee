$ ->
  unless $("#stamp_json").val() == ''
    stamp_json = JSON.parse($("#stamp_json").val())
    $.each stamp_json, (index) ->
      stamp = $("img[stamp_id='#{this.stamp_id}']")
      draggable($(stamp).parent())
      resizable(stamp)
      rotatable(this.stamp_id, this.angle)

  $("img[class='stamp']").on 'click', ->
    stamp_id = Date.now()

    wrapper = $("<div/>").attr(stamp_id: stamp_id).css(display: 'inline-block')
    draggable(wrapper)

    stamp = $("<img/>")
    stamp.attr(
      src: $(this).attr('src')
      stamp_id: stamp_id
      image_id: $(this).attr('image_id')
      height: 100
      width:100
    )

    wrapper.append(stamp)

    $("#canvas").append(wrapper)
    $(wrapper).offset($("#canvas").offset())

    resizable(stamp)
    rotatable(stamp_id)

    json = {
      stamp_id: stamp_id
      image_id: $(stamp).attr('image_id')
      top: $(stamp).position().top
      left: $(stamp).position().left
      width: $(stamp).width()
      height: $(stamp).height()
    }

    if $("#stamp_json").val() == ''
      stamp_json = []
    else
      stamp_json = JSON.parse($("#stamp_json").val())

    stamp_json.push(json)
    $("#stamp_json").val(JSON.stringify(stamp_json))

draggable = (target) ->
  $(target).draggable(
    containment: "#canvas"
    scroll: false
    stop: (event, ui) ->
      stamp_id = ui.helper.attr("stamp_id")
      canvas_offset = $("#canvas").offset()
      top  = ui.offset.top - canvas_offset.top
      left = ui.offset.left - canvas_offset.left
      console.log("move to: #{top},#{left}")
      stamp_json = JSON.parse($("#stamp_json").val())
      $.each stamp_json, (index) ->
        if parseInt(stamp_id) == $(this).attr("stamp_id")
          console.log("update json")
          stamp_json[index].top = top
          stamp_json[index].left = left

      $("#stamp_json").val(JSON.stringify(stamp_json))
  )

resizable = (target) ->
  $(target).resizable(
    stop: (event, ui) ->
      stamp_id = ui.element.parent().attr("stamp_id")
      height = ui.size.height
      width  = ui.size.width
      console.log("resize to: #{width}x#{height}")
      stamp_json = JSON.parse($("#stamp_json").val())
      $.each stamp_json, (index) ->
        if parseInt(stamp_id) == $(this).attr("stamp_id")
          console.log("update json")
          stamp_json[index].height = height
          stamp_json[index].width  = width

      $("#stamp_json").val(JSON.stringify(stamp_json))
  )

rotatable = (stamp_id, angle = 0) ->
  $("div[stamp_id='#{stamp_id}']").rotatable(
    angle: angle
    stop: (event, ui) ->
      angle = ui.angle.stop
      console.log("rotate to: #{angle}")
      stamp_json = JSON.parse($("#stamp_json").val())
      $.each stamp_json, (index) ->
        if parseInt(stamp_id) == $(this).attr("stamp_id")
          console.log("update json")
          stamp_json[index].angle = angle
      $("#stamp_json").val(JSON.stringify(stamp_json))
  )