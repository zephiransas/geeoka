module ApplicationHelper
  def error_messages(*args)
    @error_count = 0
    @error_messages = []
    args.each do |arg|
      next if arg.nil?
      @error_count += arg.errors.count
      arg.errors.full_messages.each do |message|
        @error_messages << message
      end
    end
    render :partial => 'shared/error_messages'
  end
end
