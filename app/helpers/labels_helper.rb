module LabelsHelper
  def stamp_image_tag(stamp)
    style_value = "position:absolute; top:#{stamp.top}px; left:#{stamp.left}px;"
    content_tag :div, class: 'stamp_border', style: style_value, stamp_id: stamp.id do
      image_tag "stamps/#{stamp.image_id}", width: stamp.width, height: stamp.height, stamp_id: stamp.id
    end
  end
end